#include <sourcemod>
#include <mapchooser_extended>

public Plugin myinfo = 
{
	name = "Force Map End",
	author = "Zipcore, 1NutWunDeR, Fusion",
	description = "",
	version = "1.0",
	url = ""
}

public OnPluginStart()
{
	CreateTimer(1.0, CheckRemainingTime, INVALID_HANDLE, TIMER_REPEAT);
}

public Action CheckRemainingTime(Handle timer)
{
	Handle hTmp;	
	hTmp = FindConVar("mp_timelimit");
	int iTimeLimit = GetConVarInt(hTmp);			
	if (hTmp != INVALID_HANDLE)
		CloseHandle(hTmp);	
	if (iTimeLimit > 0)
	{
		int timeleft;
		GetMapTimeLeft(timeleft);
		
		switch(timeleft)
		{
			case 1800: PrintToChatAll("\x07%06XTime remaining: \x0B30 minutes", CHAT_PREFIX);
			case 1200: PrintToChatAll("\x07%06XTime remaining: \x0B20 minutes", CHAT_PREFIX);
			case 600: PrintToChatAll("\x07%06XTime remaining: \x0B10 minutes", CHAT_PREFIX);
			case 300: PrintToChatAll("\x07%06XTime remaining: \x0B5 minutes", CHAT_PREFIX);
			case 120: PrintToChatAll("\x07%06XTime remaining: \x0B2 minutes", CHAT_PREFIX);
			case 60: PrintToChatAll("\x07%06XTime remaining: \x0B60 seconds", CHAT_PREFIX);
			case 30: PrintToChatAll("\x07%06XTime remaining: \x0B30 seconds", CHAT_PREFIX);
			case 15: PrintToChatAll("\x07%06XTime remaining: \x0B15 seconds", CHAT_PREFIX);
			case -1: PrintToChatAll("\x07%06X\x0B3..", CHAT_PREFIX);
			case -2: PrintToChatAll("\x07%06X\x0B2..", CHAT_PREFIX);
			case -3: PrintToChatAll("\x07%06X\x0B1..", CHAT_PREFIX);
		}
		
		if(timeleft < -3)
		{
			Handle hNextmap = FindConVar("sm_nextmap")
			if (hNextmap == INVALID_HANDLE)
			{	
				LogError("FATAL: Cannot find sm_nextmap cvar.");
				SetFailState("sm_nextmap not found");
			}
			
			char nextmap[128];
			GetConVarString(hNextmap, nextmap, sizeof(nextmap))
			if (!IsMapValid(nextmap))
			{
				PrintToServer("AutoChangeMap : sm_nextmap ('%s') does not contain valid map name.  Reloading current map...", nextmap)
				GetCurrentMap(nextmap, sizeof(nextmap))
				SetConVarString(hNextmap, nextmap)
			}
			
			ServerCommand("changelevel %s", nextmap)
		}
	}
}