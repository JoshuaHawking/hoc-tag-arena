public void E_PreRound2(Handle event, const char[] szName, bool dontBroadcast)
{
	g_bPreroundCalled = true;
	
	if (g_bSpecialRoundChecked)
		return;
	
	// Round countdown has started, lets check whether this round will be a special round.	
	// Check whether there has been enough round between the last round and minimum special round
	if (g_iRoundsSinceLastSpecial >= g_ConVar_MinRoundsBetweenSpecial.IntValue)
	{
		// Roll a dice, lets see whether it's a special round
		float randomNumber = Math_GetRandomFloat(0.0, 1.0);
		
		if (g_ConVar_ProbabilityForSpecial.FloatValue >= randomNumber)
		{
			// Pick a special round
			//g_iSpecialRoundType = Math_GetRandomInt(1, ROUNDTYPE_TOTAL - 1);
			g_iSpecialRoundType = ROUNDTYPE_ALLVERSUSONE;
			g_bIsSpecialRound = true;
			g_bSpecialRoundChecked = true;
			g_iRoundsSinceLastSpecial = 0;
			
			// Let the players now
			
			CPrintToChatAll(" ")
			CPrintToChatAll("{red}SPECIAL ROUND!")
			
			switch (g_iSpecialRoundType)
			{
				case (ROUNDTYPE_LOWGRAV):
				{
					CPrintToChatAll("{red}Your gravity is lowered!");
				}
				case (ROUNDTYPE_ALLVERSUSONE):
				{
					CPrintToChatAll("{red}All versus one! One player will be selected as a hider and the others will be taggers!");
					CPrintToChatAll("{red}The hider must run and hide as well as they can for a very short round.");
					
					// Change round time, has to be done in here as it's before the start
					SetConVarInt(g_ConVar_RoundTime, 1);
					// Disable autobalance so it doesn't mess with the round selection
					SetConVarBool(g_ConVar_AutoBalance, false);
				}
			}
			CPrintToChatAll(" ")
			
			return;
		}
	}
	
	// Not a special round
	g_bIsSpecialRound = false;
	g_bSpecialRoundChecked = true;
	g_iSpecialRoundType = ROUNDTYPE_NORMAL;
	g_iRoundsSinceLastSpecial++;
	
	return;
}

public E_RoundStart2(Handle event, const char[] szName, bool dontBroadcast)
{
	// This ensures that we are listening for the second preround, rather than the first
	if (!g_bPreroundCalled)
		return;
	
	// Apply custom round settings
	if (g_bIsSpecialRound)
	{
		switch (g_iSpecialRoundType)
		{
			case (ROUNDTYPE_LOWGRAV):
			{
				if (g_ConVar_Gravity != null)
					SetConVarInt(g_ConVar_Gravity, 250, true, false);
			}
			case (ROUNDTYPE_ALLVERSUSONE):
			{
				// Select one person to be the seeker
				int clients[TA_MAXPLAYERS], clientCount;
				
				for (new i = 1; i <= MaxClients; i++)
				{
					if (IsClientInGame(i) && IsPlayerAlive(i) && !IsClientTagger(i))
						clients[clientCount++] = i;
				}
				
				// There is only 1 hider avaliable, just let the game play out
				if (clientCount == 1)
					return;
				
				// Random client
				int hider = clients[GetRandomInt(0, clientCount - 1)];
				
				g_iAllVersusOneHider = hider;
				
				SetHudTextParams(-1.0, 0.25, 15.0, 173, 41, 52, 255, 0, 0.0, 0.2, 1.0);
				ShowHudText(hider, CHANNEL_SPECIALROUND, "You are being hunted down. Hide!");
				
				// Iterate through other players, setting them to taggers
				for (new i = 1; i <= MaxClients; i++)
				{
					if (IsClientInGame(i) && i != hider)
					{
						SetEntProp(i, Prop_Send, "m_bTagged", true);
						
						if (IsPlayerAlive(i))
						{
							ForcePlayerSuicide(i);
							ShowHudText(i, CHANNEL_SPECIALROUND, "You are hunting down %N!", hider);
						}
					}
				}
				
			}
		}
	}
}

public E_RoundEnd2(Handle event, const char[] szName, bool dontBroadcast)
{
	// This function is fired for multiple players, so we must only do this once.
	if (g_bIsSpecialRound)
	{
		switch (g_iSpecialRoundType)
		{
			case (ROUNDTYPE_LOWGRAV):
			{
				if (g_ConVar_Gravity != null)
					SetConVarInt(g_ConVar_Gravity, 800, true, false);
			}
			case (ROUNDTYPE_ALLVERSUSONE):
			{
				SetConVarInt(g_ConVar_RoundTime, g_iDefaultRoundTime);
				SetConVarBool(g_ConVar_AutoBalance, g_bDefaultAutoBalance);
				g_iAllVersusOneHider = -1;
			}
		}
	}
	
	// Reset special round, gravity etc.
	g_bIsSpecialRound = false;
	g_iSpecialRoundType = ROUNDTYPE_NORMAL;
	g_bSpecialRoundChecked = false;
	g_bPreroundCalled = false;
}

public E_PlayerSpawn2(Handle event, const char[] szName, bool dontBroadcast)
{
	int client = GetClientOfUserId(GetEventInt(event, "userid"));
	
	if (client < 1)
		return;
	
	// Check if it's a special round, if so, apply certain effects
	if (g_bIsSpecialRound)
	{
		switch (g_iSpecialRoundType)
		{
			case (ROUNDTYPE_ALLVERSUSONE):
			{
				// Check whether they are the hider (lol idk why this would happen, but yolo)
				if (g_iAllVersusOneHider != client && !IsClientTagger(client))
				{
					// Set them to tagger and suicide
					SetEntProp(client, Prop_Send, "m_bTagged", true);
					
					if (IsPlayerAlive(client))
					{
						ForcePlayerSuicide(client);
						SetHudTextParams(-1.0, 0.25, 15.0, 173, 41, 52, 255, 0, 0.0, 0.2, 1.0);
						ShowHudText(client, CHANNEL_SPECIALROUND, "You are hunting down %N!", g_iAllVersusOneHider);
					}
				}
			}
		}
	}
} 