/*
	Event Tracking
*/

public void E_PlayerTagged(Handle event, const char[] szName, bool dontBroadcast)
{
	int tagger = GetClientOfUserId(GetEventInt(event, "tagger"));
	
	if (tagger != 0)
	{
		if (GetEventBool(event, "ninjatag"))
		{
			g_iTotalNinjaTags[tagger]++;
		}
		else
		{
			// Add to tagger stats
			g_iTotalTags[tagger]++;
		}
		
		g_iRoundTag[tagger]++;
		
		if(g_iRoundTag[tagger] == 3)
		{
			// Play impressive sound
			EmitSoundToAll(IMPRESSIVE_SOUND_PATH);  
			CPrintToChatAll("{red}%N has tagged 3 people this round!", tagger)
		}
		else if(g_iRoundTag[tagger] == 5)
		{
			// Play impressive sound
			EmitSoundToAll(GODLIKE_SOUND_PATH);  
			CPrintToChatAll("{red}%N is GODLIKE! They've tagged 5 people this round!", tagger)
		}
		else if(g_iRoundTag[tagger] == 7)
		{
			// Play impressive sound
			EmitSoundToAll(HOLYSHIT_SOUND_PATH);  
			CPrintToChatAll("{red}HOLY SHIT! %N has tagged 7 people this round!", tagger)
		}
	}
}

public void E_RoundEnd(Handle event, const char[] szName, bool dontBroadcast)
{
	int client = GetClientOfUserId(GetEventInt(event, "userid"));
	
	if (client != 0)
	{
		bool winner = GetEventBool(event, "winner");
		
		// Get whether they were hider or seeker
		
		if (winner)
		{
			g_iHiderWins[client]++;
		}
		else
		{
			g_iHiderLosses[client]++;
		}
		
		// Add to their round count
		g_iTotalRounds[client]++;
		
		// Clear round tag count
		g_iRoundTag[client] = 0;
	}
	
	// Wipe hud
	SetHudTextParams(0.025, 0.15, 1.0, 255, 255, 255, 255, 0, 0.0, 0.0, 0.0);
	ShowHudText(client, CHANNEL_INSTRUCTIONS, " ");
}

public void E_PowerupUsed(Handle event, const char[] szName, bool dontBroadcast)
{
	int client = GetClientOfUserId(GetEventInt(event, "userid"));
	
	if (client != 0)
	{
		g_iPowerupsUsed[client]++;
	}
}

public void E_PlayerSpawn(Handle event, const char[] szName, bool dontBroadcast)
{
	// Draw instructions 
	CreateTimer(1.5, Timer_AddHUDInstructions, GetEventInt(event, "userid"));
	
	return;
}