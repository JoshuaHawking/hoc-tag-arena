/*
	Database Interaction
*/

DB_LoadDatabase()
{
	char szError[128];
	
	// Connect to database
	if (SQL_CheckConfig(MYSQL_CONFIG_NAME))
	{
		g_hDB = SQL_Connect(MYSQL_CONFIG_NAME, true, szError, sizeof(szError));
	}
	
	if (g_hDB == null)
	{
		SetFailState("Couldn't retrieve database handle!");
	}
	
	// Create user table
	SQL_TQuery(g_hDB, Thrd_Empty, "CREATE TABLE IF NOT EXISTS "...MYSQL_TABLE_USERS..." (`steamid` varchar(63) NOT NULL,`name` varchar(62) DEFAULT NULL,`firstconnected` datetime DEFAULT NULL,`lastconnected` datetime DEFAULT NULL,PRIMARY KEY (`steamid`),UNIQUE KEY `auth_UNIQUE` (`steamid`));", _, DBPrio_High);
	
	// Create stats table
	SQL_TQuery(g_hDB, Thrd_Empty, "CREATE TABLE IF NOT EXISTS "...MYSQL_TABLE_STATS..." (`steamid` varchar(63) NOT NULL,`totaltags` INT NULL,`totalninjatags` INT NULL,`totalrounds` INT NULL, `hiderwins` INT NULL, `hiderlosses` INT NULL,`seekerwins` INT NULL,`seekerlosses` INT NULL, `powerupsused` INT NULL, PRIMARY KEY (`steamid`),UNIQUE KEY `auth_UNIQUE` (`steamid`));", _, DBPrio_High);
	
}

stock bool DB_GetEscaped(char[] out, int len, const char[] def = "")
{
	if (!SQL_EscapeString(g_hDB, out, out, len))
	{
		strcopy(out, len, def);
		
		return false;
	}
	
	return true;
}

DB_LoadClient(int client)
{
	if (g_hDB == null)
	{
		return;
	}
	
	char szAuth[MAX_STEAMAUTH_LENGTH];
	
	if (!GetClientAuthId(client, AuthId_Steam3, szAuth, sizeof(szAuth)))
	{
		return;
	}
	
	char szName[62];
	GetClientName(client, szName, sizeof(szName));
	DB_GetEscaped(szName, (sizeof(szName) * 2) + 1, "N/A");
	
	char query[512];
	
	// Insert user into DB
	FormatEx(query, sizeof(query), "INSERT INTO "...MYSQL_TABLE_USERS..." (steamid, name, firstconnected, lastconnected) VALUES ('%s', '%s', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP) ON DUPLICATE KEY UPDATE name='%s', lastconnected = CURRENT_TIMESTAMP", szAuth, szName, szName);
	
	SQL_TQuery(g_hDB, Thrd_Empty, query, _, DBPrio_Normal);
	
	// Get user stats
	FormatEx(query, sizeof(query), "SELECT `totaltags`, `totalninjatags`, `totalrounds`, `hiderwins`, `hiderlosses`, `seekerwins`, `seekerlosses`,`powerupsused`,`xp` FROM "...MYSQL_TABLE_STATS..." WHERE steamid = '%s'", szAuth);
	
	SQL_TQuery(g_hDB, Thrd_GetClientStats, query, GetClientUserId(client), DBPrio_Normal);
	return;
}

public void Thrd_GetClientStats(Handle db, Handle res, const char[] szError, int userId)
{
	if (res == null)
	{
		LogError("Failed database query: failed to get stats for client.");
		return;
	}
	
	int client;
	
	// User has disconnected
	if (!(client = GetClientOfUserId(userId)))
	{
		return;
	}
	
	while (SQL_FetchRow(res))
	{
		g_iTotalTags[client] = SQL_FetchInt(res, 0);
		g_iTotalNinjaTags[client] = SQL_FetchInt(res, 1);
		g_iTotalRounds[client] = SQL_FetchInt(res, 2);
		g_iHiderWins[client] = SQL_FetchInt(res, 3);
		g_iHiderLosses[client] = SQL_FetchInt(res, 4);
		g_iSeekerWins[client] = SQL_FetchInt(res, 5);
		g_iSeekerLosses[client] = SQL_FetchInt(res, 6);
		g_iPowerupsUsed[client] = SQL_FetchInt(res, 7);
		g_iXP[client] = SQL_FetchInt(res, 8);
		
		LogAction(-1, -1, "Loaded!");
		
	}
	
	CreateTimer(2.5, Timer_WelcomeMessage, userId, TIMER_FLAG_NO_MAPCHANGE);
		
	return;
}

// Save client
DB_SaveClient(int client)
{
	if (g_hDB == null)
	{
		return;
	}
	
	// Get auth
	char szAuth[MAX_STEAMAUTH_LENGTH];
	
	if (!GetClientAuthId(client, AuthId_Steam3, szAuth, sizeof(szAuth)))
	{
		return;
	}
	
	// Build query
	char query[512];
	
	FormatEx(query, sizeof(query), 
		"INSERT INTO "...MYSQL_TABLE_STATS..." (steamid, totaltags, totalninjatags, totalrounds, hiderwins, hiderlosses, seekerwins, seekerlosses, powerupsused, xp) "...
		"VALUES ('%s', %i, %i, %i, %i, %i, %i, %i, %i, %i) "...
		"ON DUPLICATE KEY UPDATE "...
		"totaltags=%i, totalninjatags=%i, totalrounds=%i, hiderwins=%i, hiderlosses=%i, seekerwins=%i, seekerlosses=%i, powerupsused=%i, xp=%i", 
		szAuth, 
		g_iTotalTags[client], g_iTotalNinjaTags[client], g_iTotalRounds[client], g_iHiderWins[client], g_iHiderLosses[client], 
		g_iSeekerWins[client], g_iSeekerLosses[client], g_iPowerupsUsed[client], g_iXP[client],
		g_iTotalTags[client], g_iTotalNinjaTags[client], g_iTotalRounds[client], g_iHiderWins[client], g_iHiderLosses[client], 
		g_iSeekerWins[client], g_iSeekerLosses[client], g_iPowerupsUsed[client], g_iXP[client]
		);
	
	SQL_TQuery(g_hDB, Thrd_Empty, query, _, DBPrio_High);
}

public void Thrd_Empty(Handle db, Handle res, const char[] szError, any data)
{
} 