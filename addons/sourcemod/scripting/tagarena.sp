#include <sourcemod>
#include <sdktools>

#include <morecolors>
#include <smlib>
#include <chat-processor>

public Plugin myinfo = 
{
	name = "TagArena HOC", 
	author = "Josh - House of Climb", 
	description = "Keeps track of player stats", 
	version = "1.0.0", 
	url = "houseofclimb.com"
};

// Defines
#define MYSQL_CONFIG_NAME 		"tagarena_db"
#define MYSQL_TABLE_USERS 		"tagarena_users"
#define MYSQL_TABLE_STATS		"tagarena_stats"
#define TA_MAXPLAYERS 			MAXPLAYERS + 1
#define MAX_STEAMAUTH_LENGTH	21

// Sounds
#define IMPRESSIVE_SOUND_PATH	"quake/impressive_kz.mp3"
#define GODLIKE_SOUND_PATH		"quake/godlike.mp3"
#define HOLYSHIT_SOUND_PATH		"quake/holyshit_new.mp3"

// HUD
#define CHANNEL_INSTRUCTIONS	1
#define CHANNEL_SPECIALROUND 	2

// Rounds
enum
{
	ROUNDTYPE_NORMAL = 0, 
	ROUNDTYPE_LOWGRAV, 
	ROUNDTYPE_ALLVERSUSONE, 
	
	ROUNDTYPE_TOTAL
}

// CVars
ConVar g_ConVar_MinRoundsBetweenSpecial;
ConVar g_ConVar_ProbabilityForSpecial;

ConVar g_ConVar_Gravity;
ConVar g_ConVar_RoundTime;
ConVar g_ConVar_AutoBalance;
ConVar g_ConVar_Tags;

// Globals
Handle g_hDB;

int g_iDefaultRoundTime;
bool g_bDefaultAutoBalance;

// Stats
int g_iTotalTags[TA_MAXPLAYERS];
int g_iTotalNinjaTags[TA_MAXPLAYERS];
int g_iTotalRounds[TA_MAXPLAYERS];
int g_iHiderWins[TA_MAXPLAYERS];
int g_iHiderLosses[TA_MAXPLAYERS];
int g_iSeekerWins[TA_MAXPLAYERS];
int g_iSeekerLosses[TA_MAXPLAYERS];
int g_iPowerupsUsed[TA_MAXPLAYERS];
int g_iXP[TA_MAXPLAYERS];

int g_iRoundTag[TA_MAXPLAYERS]; // Used to track the amount of tags for a player based on the round

bool g_bPreroundCalled;

// Special Rounds
bool g_bIsSpecialRound;
bool g_bSpecialRoundChecked;
int g_iSpecialRoundType;
int g_iRoundsSinceLastSpecial = 0;

// Specific special round details
int g_iAllVersusOneHider;

// Includes
#include <tagarena/db.sp>
#include <tagarena/events.sp>
#include <tagarena/specialrounds.sp>

public void OnPluginStart()
{
	
	// Stats
	HookEvent("player_spawn", E_PlayerSpawn);
	HookEvent("tagarena_tagged_event", E_PlayerTagged);
	HookEvent("tagarena_round_end", E_RoundEnd);
	HookEvent("player_usepowerup", E_PowerupUsed)
	
	// Rounds
	HookEvent("tagarena_preround", E_PreRound2);
	HookEvent("tagarena_start_round", E_RoundStart2);
	HookEvent("tagarena_round_end", E_RoundEnd2);
	HookEvent("player_spawn", E_PlayerSpawn2);
	
	// Commands
	RegConsoleCmd("sm_stats", Cmd_Stats);
	
	// CVars
	g_ConVar_MinRoundsBetweenSpecial = CreateConVar("kz_sv_tagarena_setting_minimumroundsbetweenspecial", "4", "The minimum amount of rounds between special rounds.");
	g_ConVar_ProbabilityForSpecial = CreateConVar("kz_sv_tagarena_setting_probability", "0.25", "The probability that a special round will occur if possible (assuming minimum rounds has been passed etc.).", _, true, 0.0, true, 1.0);
	
	g_ConVar_Gravity = FindConVar("sv_gravity");
	SetConVarInt(g_ConVar_Gravity, 800); // Force reset
	
	g_ConVar_RoundTime = FindConVar("kz_sv_tagarena_roundtime");
	g_ConVar_AutoBalance = FindConVar("kz_sv_tagarena_autobalance");
	g_ConVar_Tags = FindConVar("sv_tags");
	
	// Disable notify flag
	RemoveConVarFlag(g_ConVar_Gravity, FCVAR_NOTIFY);
	RemoveConVarFlag(g_ConVar_Tags, FCVAR_NOTIFY);
	RemoveConVarFlag(g_ConVar_AutoBalance, FCVAR_NOTIFY);
	RemoveConVarFlag(g_ConVar_RoundTime, FCVAR_NOTIFY);
	
	g_iDefaultRoundTime = GetConVarInt(g_ConVar_RoundTime);
	g_bDefaultAutoBalance = GetConVarBool(g_ConVar_AutoBalance);
	
	// Initiate database connection
	DB_LoadDatabase();
}

public void OnMapStart()
{
	// Add download tables
	AddFileToDownloadsTable("sound/"...IMPRESSIVE_SOUND_PATH);
	PrecacheSound(IMPRESSIVE_SOUND_PATH);
	AddFileToDownloadsTable("sound/"...GODLIKE_SOUND_PATH);
	PrecacheSound(GODLIKE_SOUND_PATH);
	AddFileToDownloadsTable("sound/"...HOLYSHIT_SOUND_PATH);
	PrecacheSound(HOLYSHIT_SOUND_PATH);
	
	LogAction(-1, -1, "Added files to download table.");
}

public void OnClientConnected(int client)
{
	g_iTotalTags[client] = 0;
	g_iTotalNinjaTags[client] = 0;
	g_iTotalRounds[client] = 0;
	g_iHiderWins[client] = 0;
	g_iHiderLosses[client] = 0;
	g_iSeekerWins[client] = 0;
	g_iSeekerLosses[client] = 0;
	g_iPowerupsUsed[client] = 0;
	g_iXP[client] = 0;
}

public Action OnChatMessage(int & author, ArrayList recipients, eChatFlags & flag, char[] name, char[] message, bool & bProcessColors, bool & bRemoveColors)
{
	if (IsClientAuthorized(author) && CheckCommandAccess(author, "sm_admin", ADMFLAG_CHANGEMAP))
	{
		char name2[MAX_NAME_LENGTH];
		//strcopy(name2, MAX_NAME_LENGTH, name);
		FormatEx(name2, 128, "{red}Admin {teamcolor}%s{default}", name);
		CReplaceColorCodes(name2, author, false, 128);
		
		strcopy(name, MAX_NAME_LENGTH, name2);
		
		return Plugin_Changed;
	}
	
	return Plugin_Continue;
}

public void OnClientAuthorized(int client, const char[] auth)
{
	// Load client
	DB_LoadClient(client);
}

public void OnClientDisconnect(int client)
{
	// Save client stats
	DB_SaveClient(client);
}

/*
	Commands
*/

public Action Cmd_Stats(int client, int args)
{
	// Print stats
	CPrintToChat(client, "{green}Your Stats");
	CPrintToChat(client, "Total Rounds Played: %i", g_iTotalRounds[client]);
	CPrintToChat(client, "Total Tags: %i | Ninja tags: %i", g_iTotalTags[client], g_iTotalNinjaTags[client]);
	CPrintToChat(client, "Wins / Losses:{green} %i win%s {default}/{red} %i loss%s", g_iHiderWins[client], (g_iHiderWins[client] == 1 ? "" : "s"), g_iHiderLosses[client], (g_iHiderLosses[client] == 1 ? "" : "es"));
	//CPrintToChat(client, "Seeker Stats: {green} %i {default}/{red} %i", g_iSeekerWins[client], g_iSeekerLosses[client]);
	CPrintToChat(client, "Powerups Used: %i", g_iPowerupsUsed[client]);
	CPrintToChat(client, " ");
	
	return Plugin_Handled;
}

/*
	Timer
*/

Action Timer_WelcomeMessage(Handle timer, int userId)
{
	int client;
	
	// User has disconnected
	if (!(client = GetClientOfUserId(userId)))
	{
		return;
	}
	
	if (!IsClientInGame(client))
	{
		return;
	}
	
	// After they have loaded, let them know of their stats
	CPrintToChat(client, " ");
	CPrintToChat(client, " ");
	CPrintToChat(client, " ");
	CPrintToChat(client, "{green}Welcome to House of Climb Tag Arena!");
	CPrintToChat(client, "You've played {green}%i {default}rounds! Do !stats to view other stats!", g_iTotalRounds[client]);
	CPrintToChat(client, " ");
	CPrintToChat(client, "We are still in very early stages of development!")
	CPrintToChat(client, "Check us out at houseofclimb.com!");
}

Action Timer_AddHUDInstructions(Handle timer, int userId)
{
	int client;
	
	// User has disconnected
	if (!(client = GetClientOfUserId(userId)))
	{
		return;
	}
	
	if (!IsClientInGame(client))
	{
		return;
	}
	
	// Check whether they are a tagger
	if (IsClientTagger(client))
	{
		SetHudTextParams(0.025, 0.15, 45.0, 255, 0, 0, 255, 0, 0.0, 0.0, 1.0);
		ShowHudText(client, CHANNEL_INSTRUCTIONS, "You are a seeker!\n- Knife hiders to tag them!\n- Hold down right click to use your powerjump!");
	}
	else
	{
		SetHudTextParams(0.025, 0.15, 45.0, 0, 255, 0, 255, 0, 0.0, 0.0, 1.0);
		ShowHudText(client, CHANNEL_INSTRUCTIONS, "You are a hider!\n- Run from the seekers!\n- Press '1' to use your powerups!");
	}
}

stock IsClientTagger(int client)
{
	return GetEntProp(client, Prop_Send, "m_bTagged");
} 

stock RemoveConVarFlag(ConVar convar, int flag)
{
	int flags = convar.Flags;
	flags |= flag;
	convar.Flags = flags;
}
